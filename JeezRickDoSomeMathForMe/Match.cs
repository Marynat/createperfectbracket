﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeezRickDoSomeMathForMe
{
    /// <summary>
    /// Class of a match, can be made to extend your normal class but the Match prevMatchA and b are neccesary
    /// </summary>
    class Match
    {
        public int id;
        public Team better;
        public Team worse;
        public Match prevMatchA;
        public Match prevMatchB;
        public Team teamWinner;


        public Match(int _id, Team b, Team w)
        {
            id = _id;
            this.better = b;
            this.worse = w;
            this.teamWinner = this.better; //for start
        }
    }
}
