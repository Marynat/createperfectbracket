﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeezRickDoSomeMathForMe
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Team> teams = new List<Team>();
            Helper helpME = new Helper();
            Initialize(teams);
            formRounds(new List<Match>(), teams, helpME);
            List<int> idOfMatches = new List<int>();

            PrintRounds(helpME.rounds.Last(), idOfMatches); //gets the list and adds correct list to it.

            Console.WriteLine("ARRAAAAAAAAAAAAAAAAAAAAAAAAY");

            foreach (int i in idOfMatches)
            {
                Console.WriteLine(i);
            }
            Console.ReadKey();
        }


        public static void Initialize(List<Team> teams)
        {
            //Person p1 = new Person("p1", 2200);
            //Person p2 = new Person("p2", 2200);
            //Person p3 = new Person("p3", 2200);
            //Person p4 = new Person("p4", 2200);
            //Person p5 = new Person("p5", 2200);
            //Person p6 = new Person("p6", 2200);
            //Person p7 = new Person("p7", 2200);
            //Person p8 = new Person("p8", 2200);
            //Person p9 = new Person("p9", 2200);
            //Person p10 = new Person("p10", 2200);
            //Person p11 = new Person("p11", 2200);
            //Person p12 = new Person("p12", 2200);
            //Person p13 = new Person("p13", 2200);
            //Person p14 = new Person("p14", 2200);
            //Person p15 = new Person("p15", 2200);
            //Person p16 = new Person("p16", 2200);
            //Person p17 = new Person("p17", 2200);
            //Person p18 = new Person("p18", 2200);
            //Person p19 = new Person("p19", 2200);
            //Person p20 = new Person("p20", 2200);
            //Person p21 = new Person("p21", 2200);
            //Person p22 = new Person("p22", 2200);
            //Person p23 = new Person("p23", 2200);
            //Person p24 = new Person("p24", 2200);
            //Person p25 = new Person("p25", 2200);
            //Person p26 = new Person("p26", 2200);
            //Person p27 = new Person("p27", 2200);
            //Person p28 = new Person("p28", 2200);
            //Person p29 = new Person("p29", 2200);
            //Person p30 = new Person("p30", 2200);
            //Person p31 = new Person("p31", 2200);
            //Person p32 = new Person("p32", 2200);
            //Person p33 = new Person("p1", 2200);
            //Person p34 = new Person("p2", 2200);
            //Person p35 = new Person("p3", 2200);
            //Person p36 = new Person("p4", 2200);
            //Person p37 = new Person("p5", 2200);
            //Person p38 = new Person("p6", 2200);
            //Person p39 = new Person("p7", 2200);
            //Person p40 = new Person("p8", 2200);
            //Person p41 = new Person("p9", 2200);
            //Person p42 = new Person("p10", 2200);
            //Person p43 = new Person("p11", 2200);
            //Person p44 = new Person("p12", 2200);
            //Person p45 = new Person("p13", 2200);


            teams.Add(new Team(0, "t0"));
            teams.Add(new Team(1, "t0"));
            teams.Add(new Team(2, "t1"));
            teams.Add(new Team(3, "t2"));
            teams.Add(new Team(4, "t3"));
            teams.Add(new Team(5, "t4"));
            teams.Add(new Team(6, "t5"));
            teams.Add(new Team(7, "t6"));
            teams.Add(new Team(8, "t7"));
            teams.Add(new Team(9, "t8"));
            teams.Add(new Team(10, "t9"));
            teams.Add(new Team(11, "t10"));
            teams.Add(new Team(12, "t11"));
            teams.Add(new Team(13, "t12"));
            teams.Add(new Team(14, "t13"));
            teams.Add(new Team(15, "t14"));
            teams.Add(new Team(16, "t15"));
            teams.Add(new Team(17, "t16"));
            teams.Add(new Team(18, "t17"));
            teams.Add(new Team(19, "t18"));
            teams.Add(new Team(20, "t19"));
            teams.Add(new Team(21, "t20"));
            teams.Add(new Team(22, "t21"));
            teams.Add(new Team(23, "t22"));
            teams.Add(new Team(24, "t23"));
            teams.Add(new Team(25, "t24"));
            teams.Add(new Team(26, "t25"));
            teams.Add(new Team(27, "t26"));
            teams.Add(new Team(28, "t27"));
            teams.Add(new Team(29, "t28"));
            teams.Add(new Team(30, "t29"));
            teams.Add(new Team(31, "t30"));
            //teams.Add(new Team(32, "t31"));





        }


        public static void formRounds(List<Match> _matches, List<Team> teams, Helper h)
        {
            Round r = new Round(h.rounds.Count, new List<Match>());
            if (h.rounds.Count == 0)
            {
                if (_matches.Count == 0)
                {
                    for (int i = 0; i < teams.Count / 2; i++)
                    {
                        Match m = new Match(i, teams[i], teams[teams.Count - 1 - i]);
                        r.matches.Add(m);
                        h.totalMatches++;
                    }
                }
                else
                {
                    h.totalMatches = _matches.Count;
                    r.matches = _matches; //most likely wont work but some function to copy one list to another
                }
                h.rounds.Add(r);
                formRounds(r.matches, teams, h);
            }
            else
            {
                Round prevRound = new Round(h.rounds.Count, new List<Match>());
                prevRound.roundNo = h.rounds.Count - 1;
                List<Match> matchesInPrevRound = h.rounds[prevRound.roundNo].matches; //same here

                for (int j = 0; j < matchesInPrevRound.Count / 2; j++)
                {
                    h.totalMatches++;
                    Match m = new Match(h.totalMatches, matchesInPrevRound[j].teamWinner, matchesInPrevRound[matchesInPrevRound.Count - 1 - j].teamWinner);
                    m.prevMatchA = matchesInPrevRound[j];
                    m.prevMatchB = matchesInPrevRound[matchesInPrevRound.Count - 1 - j];
                    r.matches.Add(m);
                }
                r.roundNo = h.rounds.Count;
                if (matchesInPrevRound.Count == 2)
                {
                    return;
                }
                else
                {
                    h.rounds.Add(r);
                    formRounds(r.matches, teams, h);
                }
                h.totalMatches++;
                //h.rounds.Add(new Round(h.rounds.Count - 1, new List<Match> { new Match(h.totalMatches, r.matches[0].teamWinner, r.matches[1].teamWinner) }));
            }
        }
        public static void PrintRounds(Round r, List<int> arr)
        {
            foreach (Match m in r.matches)
            {
                PrintMatchesRecursevly(m, arr);
            }
            Console.ReadKey();
        }

        public static void PrintMatchesRecursevly(Match m, List<int> arr)
        {
            if (m.prevMatchA != null)
            {

                PrintMatchesRecursevly(m.prevMatchA, arr);
                PrintMatchesRecursevly(m.prevMatchB, arr);

            }
            else
            {
                Console.WriteLine(m.better.id);
                arr.Add(m.better.id);
                Console.WriteLine(m.worse.id);
                arr.Add(m.worse.id);
                return;
            }
        }
    }
}
