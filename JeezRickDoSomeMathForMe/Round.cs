﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeezRickDoSomeMathForMe
{

    /// <summary>
    /// Class that helps with tranition between matches in round;
    /// </summary>
    class Round
    {

         public int roundNo;
         public List<Match> matches;

        public Round(int roundNo, List<Match> matches)
        {
            this.roundNo = roundNo;
            this.matches = matches;
        }
    }
}
